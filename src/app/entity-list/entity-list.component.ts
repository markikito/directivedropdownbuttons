import { Component, OnInit } from '@angular/core';

const collections = [
  {
    id: '618bad7103c05d0126cc3b37',
    name: 'accounts',
    description: 'Representa los datos de las compañias que nos contratan.'
  },
  {
    id: '618bad7de4c76b013375197c',
    name: 'employees',
    description: 'Campos asociados a los empleados.'
  },
  {
    id: '618bad88d39a3501401482c1',
    name: 'assets',
    description: 'Campos asociados a los recursos tecnológicos'
  },
  {
    id: '618bad95981153014d0a16e5',
    name: 'procedures',
    description: 'Campos asociados a los procedimientos de la compañia.'
  },
  {
    id: '618bad9f41a068015aa83992',
    name: 'providers',
    description: 'Campos asociados a los proveedores.'
  },
  {
    id: '618badc79f5eb0018ebd0a8f',
    name: 'devices',
    description: 'Campos asociados a los dispositivos de la compañia.'
  },
  {
    id: '618badd0acea4a019bba3bcd',
    name: 'applications',
    description: 'Campos asociados a las aplicaciones usadas en la compañía.'
  },
];

@Component({
  selector: 'app-list-entity',
  templateUrl: './entity-list.component.html',
  styleUrls: []
})
export class EntityListComponent implements OnInit {
  collections: { id: string, name: string, description: string }[];
  constructor() {
    this.collections = collections;
   }

  ngOnInit(): void {
  }

}
