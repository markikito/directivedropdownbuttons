import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { FormularioComponent } from './formulario/formulario.component';
import { InputsComponent } from './inputs/inputs.component';
import { EntityListComponent } from './entity-list/entity-list.component';
import { EntityDetailComponent } from './entity-detail/entity-detail.component';
import { DragAndDropComponent } from './drag-and-drop/drag-and-drop.component';

const routes: Routes = [
    { path: 'first-component', component: FirstComponent },
    { path: 'second-component', component: SecondComponent },
    { path: 'form', component: FormularioComponent },
    { path: 'toolbar', component: ToolbarComponent },
    { path: 'inputs', component: InputsComponent},
    { path: 'entity-list', component: EntityListComponent},
    { path: 'entity-detail', component: EntityDetailComponent},
    { path: 'drag-and-drop', component: DragAndDropComponent},
    
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
