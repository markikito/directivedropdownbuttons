import { Directive, TemplateRef, ViewContainerRef } from '@angular/core';
import { fromEvent, Observable, Subscription } from 'rxjs';

@Directive({
  selector: '[appToolbarDropDown]',
})
export class ToolbarDropDownDirective {
  resizeObservable$: Observable<Event>;
  resizeSubscription$: Subscription;

  constructor(private templateRef: TemplateRef<any>, private viewContainer: ViewContainerRef) {}

  ngOnInit() {
    this.viewContainer.createEmbeddedView(this.templateRef);
    this.resizeObservable$ = fromEvent(window, 'resize');
    this.resizeSubscription$ = this.resizeObservable$.subscribe((evt) => {
      this.viewContainer.clear();
      this.viewContainer.createEmbeddedView(this.templateRef);

      this.checkOverlasps();
    });
  }

  ngAfterViewInit() {
    this.checkOverlasps();
  }

  checkOverlasps() {
    console.log('INICIO checkOverlasps');
    const dropDownButton = [];
    const leftSide = document.getElementById('leftSide');
    const rigthSide = document.getElementById('rigthSide');

    console.log('rect(leftSide):', leftSide.getBoundingClientRect());
    console.log('rect(rigthSide):', rigthSide.getBoundingClientRect());

    //
    console.log('Existe el drop down menu???');
    const kk = leftSide.getElementsByClassName('dropdown-menu');
    console.log('kk is:', kk);
        
    const xMaxFromLeftSide = leftSide.getBoundingClientRect().x + leftSide.getBoundingClientRect().width;
    const xMinFromRigthSide = rigthSide.getBoundingClientRect().x;
    const moreButtonWidth = 300;

    const buttons = rigthSide.getElementsByTagName('button');
    console.log('Numero de botones:', buttons.length);
    for (let i = 0; i < buttons.length; i++) {
      const button = buttons[i];
      console.log('button is:', button);
    }

    const overlap = this.overlaps(leftSide, rigthSide);
    console.log('overlap is', overlap);
    
    if (overlap) {
      if (buttons.length > 2) {
        let removedButtonsWidth = buttons[buttons.length - 1].getBoundingClientRect().width;
        dropDownButton.push(buttons[buttons.length - 1]);
        for (let i = buttons.length - 2; i >= 0; i--) {
          const button = buttons[i];
          const buttonWidth = button.getBoundingClientRect().width;
          /*
          console.log(
            `button[${i}]: boton: ${button.innerText.trim()} ancho: ${
              button.getBoundingClientRect().width
            }`
          );
          console.log(
            `condition: ${leftSideRect.x + leftSideRect.width} <= ${
              rigthSideRect.x + button.getBoundingClientRect().width + widthButtonsRemoved - moreButtonWidth
            }`
          );
          */
          if (xMaxFromLeftSide > xMinFromRigthSide + buttonWidth + removedButtonsWidth - moreButtonWidth) {
            dropDownButton.push(button);
            removedButtonsWidth = removedButtonsWidth + buttonWidth;
          } else {
            break;
          }
        }
      }
    }

    console.log('Removed buttons', JSON.stringify(dropDownButton.reverse().map((e) => e.innerText)));
    if (dropDownButton.length > 0) {
      const moreButton = this.getMoreButton();
      rigthSide.appendChild(moreButton);
      const div = this.createDropdownMenuDiv();
      for (let i = 0; i < dropDownButton.length; i++) {
        const button = dropDownButton[i];
        rigthSide.removeChild(button);
        div.appendChild(this.createOptionButton(button));
      }
      rigthSide.appendChild(div);
    }
    console.log('FIN checkOverlasps');
  }

  overlaps(a: HTMLElement, b: HTMLElement) {
    const rect1 = a.getBoundingClientRect();
    const rect2 = b.getBoundingClientRect();
    console.log('rect1', JSON.stringify(rect1));
    console.log('rect2', JSON.stringify(rect2));
    const isInHoriztonalBounds = rect1.x + rect1.width >= rect2.x;
    const isInVerticalBounds = false;
    const isOverlapping = isInHoriztonalBounds || isInVerticalBounds;
    return isOverlapping;
  }

  getMoreButton() {
    const button = ` <button type="button" class="btn btn-outline-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="width: 80px;">More</button>`;
    return this.createElementFromString(button);
  }

  createDropdownMenuDiv() {
    const div = document.createElement('div');
    div.setAttribute('class', 'dropdown-menu');
    return div;
  }

  createOptionButton(button) {
    const label = button.innerText.trim();
    const icon = button.getElementsByTagName('i')[0].getAttribute('class');
    const optionButton = `<a class="dropdown-item" style="width: 85px;"><i class="${icon}"></i> ${label}</a>`;
    return this.createElementFromString(optionButton);
  }

  createElementFromString(text) {
    const rootNode = new DOMParser().parseFromString(text, 'text/html');
    const body = rootNode.getElementsByTagName('body')[0];
    return body.firstElementChild;
  }

  ngOnDestroy() {
    this.resizeSubscription$.unsubscribe();
  }
}
