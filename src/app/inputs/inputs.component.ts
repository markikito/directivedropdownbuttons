import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';

interface Field {
  name: string;
  label: string;
  required: boolean;
  hidden: boolean;
  type: string;
  value?: any;
}

@Component({
  selector: 'app-inputs',
  templateUrl: './inputs.component.html',
  styleUrls: ['./inputs.component.css']
})
export class InputsComponent implements OnInit {
  form: FormGroup;
  field: Field;
  field2: Field;
  constructor() {
    this.field = {
      name: "fecha",
      label: "Fecha",
      required: false,
      hidden: false,
      type: "date",
      value: "2021-11-10"
    };
    this.field2 = {
      name: "fecha",
      label: "Fecha",
      required: false,
      hidden: false,
      type: "date",
      value: "2021-11-30T07:02:14.573Z"
    };    
   }

  ngOnInit(): void {
  }

}
