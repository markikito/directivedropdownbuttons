import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FirstComponent } from './first/first.component';
import { SecondComponent } from './second/second.component';
import { DestacarDirective } from './destacar.directive';
import { MultiplicarDirective } from './multiplicar.directive';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { ToolbarDropDownDirective } from './toolbar-drop-down.directive';
import { CopyClipboardDirective } from './copy-clipboard.directive';
import { FormularioComponent } from './formulario/formulario.component';
import { InputsComponent } from './inputs/inputs.component';
import { EntityListComponent } from './entity-list/entity-list.component';
import { EntityDetailComponent } from './entity-detail/entity-detail.component';

import { DragAndDropComponent } from './drag-and-drop/drag-and-drop.component';
import { DroppableDirective } from './directives/drag-and-drop/droppable.directive';
import { DragService } from './directives/drag-and-drop/drag.service';
import { DraggableDirective } from './directives/drag-and-drop/draggable.directive';

import { FieldWindowComponent } from './entity-detail/components/field-window/field-window.component';
import { GroupWindowComponent } from './entity-detail/components/group-window/group-window.component';


@NgModule({
  declarations: [
    AppComponent,
    FirstComponent,
    SecondComponent,
    DestacarDirective,
    MultiplicarDirective,
    ToolbarComponent,
    ToolbarDropDownDirective,
    CopyClipboardDirective,
    FormularioComponent,
    InputsComponent,
    EntityListComponent,
    EntityDetailComponent,
    FieldWindowComponent,
    GroupWindowComponent,
    DragAndDropComponent,
    DroppableDirective,
    DraggableDirective,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
  ],
  providers: [DragService],
  bootstrap: [AppComponent]
})
export class AppModule { }
