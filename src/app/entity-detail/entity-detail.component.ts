import { Component, OnInit } from '@angular/core';
import { definition } from './definition';

@Component({
  selector: 'app-detail-entity',
  templateUrl: './entity-detail.component.html',
  styleUrls: ['./entity-detail.css'],
})
export class EntityDetailComponent implements OnInit {
  definition: any;
  blocks: any[];

  fieldWindowShow: boolean;
  currentField: any;

  blockWindowShow: boolean;
  currentBlock: any;

  fieldMaxIndex: number;
  blockMaxIndex: number;

  newFieldBlock: string;

  constructor() {
    this.definition = definition;
    this.fieldWindowShow = false;
    this.blockWindowShow = false;
    this.blocks = [];
    this.fieldMaxIndex = 0;
    this.blockMaxIndex = 0;
    this.newFieldBlock = null;
  }

  ngOnInit(): void {
    this.initBlocks();
  }

  /** Blocks BEGIN */
  getBlockByFieldId(fieldId: number): any {
    for (let block of this.blocks) {
      if (block.fields.find((f) => f.id == fieldId)) {
        return block;
      }
    }
    return null;
  }

  saveBlock(block: any): void {
    if (block.blockid != undefined) {
      const blockIndex = this.blocks.findIndex((b) => b.blockid == block.blockid);
      this.blocks[blockIndex].blockname = block.blockname;
      this.blocks[blockIndex].blocklabel = block.blocklabel;
    } else {
      this.blockMaxIndex++;
      const newBlock = { ...block, blockid: this.blockMaxIndex, blocksequence: this.blockMaxIndex, fields: [] };
      this.blocks.push(newBlock);
    }
  }

  editBlock(block) {
    this.blockWindowShow = true;
    this.currentBlock = block;
  }

  addNewBlock() {
    const newBlock = {
      blockid: undefined,
      blocklabel: '',
      blockname: '',
      blocksequence: undefined,
      fields: [],
    };
    this.editBlock(newBlock);
  }

  deleteBlock(block: any) {
    if (confirm('Are you sure delete block and all its fields?')) {
      const elementIndexToDelete = this.blocks.findIndex((b) => b.blockid == block.blockid);
      this.blocks.splice(elementIndexToDelete, 1);
    }
  }

  /** Blocks END */

  /** Fields BEGIN */
  saveField(field: any): void {
    if (field.id != undefined) {
      const block = this.getBlockByFieldId(field.id);
      const index = block.fields.findIndex((f) => f.id == field.id);
      const newField = Object.assign(field);
      block.fields[index] = newField;
    } else {
      const block = this.blocks.find((g) => g.blockid == this.newFieldBlock);
      this.newFieldBlock = null;
      this.fieldMaxIndex++;
      field.id = this.fieldMaxIndex;
      block.fields.push(field);
    }
  }

  editField(field) {
    this.fieldWindowShow = true;
    this.currentField = field;
  }

  addField(blockid) {
    this.newFieldBlock = blockid;
    this.fieldWindowShow = true;
    this.currentField = {
      name: '',
      label: '',
      mandatory: false,
      hidden: false,
      editable: true,
      type: { name: 'string' },
    };
  }

  deleteField(field: any) {
    if (confirm('Are you suer delete field?')) {
      const block = this.getBlockByFieldId(field.id);
      const elementIndexToDelete = block.fields.findIndex((e) => e.id === field.id);
      block.fields.splice(elementIndexToDelete, 1);
    }
  }
  /** Fields END */

  /** Drag and Drop BEGIN */
  allowDrop(ev) {
    ev.preventDefault();
  }

  drag(ev) {
    ev.dataTransfer.setData('text', ev.target.id);
  }

  drop(ev) {
    ev.preventDefault();
    const sourceFieldId = ev.dataTransfer.getData('text').split('-')[2];
    const targetFieldId = ev.currentTarget.id.split('-')[2];
    this.swapField(sourceFieldId, targetFieldId);
  }

  swapField(sourceFieldId, targetFieldId) {
    const block = this.getBlockByFieldId(sourceFieldId);
    const sourceFieldIndex = block.fields.findIndex((f) => f.id == sourceFieldId);
    const targetFieldIndex = block.fields.findIndex((f) => f.id == targetFieldId);
    const sourceFieldContent = JSON.parse(JSON.stringify(block.fields[sourceFieldIndex]));

    block.fields[sourceFieldIndex] = block.fields[targetFieldIndex];
    block.fields[targetFieldIndex] = sourceFieldContent;
  }
  /** Drag and Drop END */

  addBlock(block) {
    if (block) {
      if (block.blockid > this.blockMaxIndex) {
        this.blockMaxIndex = block.blockid;
      }
      if (this.blocks.filter((e) => e.blockid === block.blockid).length === 0) {
        block.fields = [];
        this.blocks.push(block);
      }
    }
  }

  initBlocks() {
    for (const field of this.definition.fields) {
      if (field.block) {
        this.addBlock(field.block);
        const block = this.blocks.find((b) => b.blockid === field.block.blockid);
        block.fields.push({
          name: field.name,
          label: field.label,
          mandatory: field.mandatory,
          type: field.type,
          hidden: field.hidden,
          editable: field.editable,
          sequence: field.sequence,
          id: this.fieldMaxIndex,
        });
        this.fieldMaxIndex++;
      }
    }
  }

  save() {
    console.log('blocks is:', this.blocks);
  }
}
