import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-group-window',
  templateUrl: './group-window.component.html',
  styleUrls: [],
})
export class GroupWindowComponent implements OnInit {
  @Input() show: boolean;
  @Input() block: any;

  @Output() showChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onSave: EventEmitter<any> = new EventEmitter<any>();

  myForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {
    this.myForm = this.formBuilder.group({
      blockname: ['', [Validators.required]],
      blocklabel: ['', [Validators.required]],
    });
    this.block = {
      blockname: '',
      blocklabel: ''
    };
  }

  ngOnInit(): void {
    this.myForm.patchValue({
      blockname: this.block.blockname,
      blocklabel: this.block.blocklabel,
    });
  }

  close() {
    this.myForm.reset();
    this.show = false;
    this.showChange.emit(this.show);
  }

  save() {
    this.onSave.emit({
      blockname: this.myForm.value.blockname,
      blocklabel: this.myForm.value.blocklabel,
      blockid: this.block.blockid,
      blocksequence: this.block.blocksequence
    });
    this.close();
  }
}
