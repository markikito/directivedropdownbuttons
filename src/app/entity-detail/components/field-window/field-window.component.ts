import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FieldTypes, Picklists } from '../../definition';

@Component({
  selector: 'app-field-window',
  templateUrl: './field-window.component.html',
  styleUrls: [],
})
export class FieldWindowComponent implements OnInit {
  @Input() show: boolean;
  @Input() field: any;

  @Output() showChange: EventEmitter<boolean> = new EventEmitter<boolean>();
  @Output() onSave: EventEmitter<any> = new EventEmitter<any>();

  myForm: FormGroup;

  types: any;
  picklists: any;

  constructor(private formBuilder: FormBuilder) {
    this.types = FieldTypes;
    this.picklists = Picklists;
    this.myForm = this.formBuilder.group({
      name: ['', [Validators.required]],
      label: ['', [Validators.required]],
      mandatory: ['0', [Validators.required]],
      hidden: ['0', [Validators.required]],
      editable: ['1', [Validators.required]],
      type: ['', [Validators.required]],
      picklist: [''],
    });
    this.field = {
      name: '',
      label: '',
      mandatory: '0',
      hidden: '0',
      editable: '1',
      type: 'string',
      picklist: '',
    };
  }

  ngOnInit(): void {
    const picklist =
      this.field.type.datasource && this.field.type.datasource.filterFieldValue
        ? this.field.type.datasource && this.field.type.datasource.filterFieldValue
        : '';

    this.myForm.patchValue({
      name: this.field.name,
      label: this.field.label,
      mandatory: this.field.mandatory ? '1' : '0',
      hidden: this.field.hidden ? '1' : '0',
      editable: this.field.editable ? '1' : '0',
      type: this.field.type.name,
      picklist: picklist,
    });
  }

  close() {
    this.myForm.reset();
    this.show = false;
    this.showChange.emit(this.show);
  }

  save() {
    const type = { name: this.myForm.value.type };
    if (type.name == 'picklist') {
      type['datasource'] = {
        collection: 'picklists',
        filterFieldName: 'name',
        filterFieldValue: this.myForm.value.picklist,
        optionsFieldName: 'values',
        labelFieldName: null,
        valueFieldName: null,
      };
    }
    this.onSave.emit({
      id: this.field.id,
      name: this.myForm.value.name,
      label: this.myForm.value.label,
      mandatory: this.myForm.value.mandatory == '0' ? false : true,
      hidden: this.myForm.value.hidden == '0' ? false : true,
      editable: this.myForm.value.editable == '0' ? false : true,
      type: type,
    });
    this.close();
  }
}
