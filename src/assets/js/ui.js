var UI = function ($, _) {

  window.Confirm = function (config, callback) {
    var parseConfig = function() {
      try {
        if(config && config.jquery) return config.data('confirm');
        if(typeof config === 'string') return JSON.parse(config);
        if(typeof config === 'object') return config;
      }
      catch(e) {
        return {};
      }
    };
    return swal(_.extend({   
      title: "Are you sure?",   
      text: "You will not be able to recover the selected item/s!",   
      type: "warning", 
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!"
    }, parseConfig()), callback);
  };

  window.Notification = function(config) {
    $.toast(_.extend({
      loaderBg:'#1e88e5',
      icon: 'info',
      hideAfter: 10000
    }, config));
  };

  window.Flash = function($target) {
    $target.find('[data-role=notification]').each(function(i, el) {
      Notification(_.extend({text: $(el).text()}, $(el).data()));
    });
  };

  function init () {
    $(window).on("beforeunload", function() {
      $('.preloader').fadeIn();
    });
    parse($('body'));
  };

  function parse ($target) {
    try {
      $target.find('[data-plugin="knob"]').knob();
      $target.find('[data-plugin="peity"][data-chart="donut"]').peity("donut",{width: 50, height: 50});
      $target.find('[data-plugin="peity"][data-chart="pie"]').peity("pie",{width: 50, height: 50});
      $target.find('[data-url]').click(function() {
        window.location.assign($(this).data('url'));
      });
      Flash($target);
      $target.find('[data-controller]').each(function(i, el) {
        var $el = $(el);
        try {
          window[$el.data('controller')]($el);
        } catch(e) {
          console.log("Can't initialize " + $el.data('controller'));
        }
      });      
    }
    catch(e) {}
  };

  $(init);

  return {
    parse: parse
  };

}(jQuery, _);